package ru.tsc.kitaev.tm.api.repository;

import ru.tsc.kitaev.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository <E extends AbstractOwnerEntity> extends IRepository<E> {

    E add(String userId, E entity);

    void remove(String userId, E entity);

    void clear(String userId);

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    E findById(String userId, String id);

    E findByIndex(String userId, Integer index);

    E removeById(String userId, String id);

    E removeByIndex(String userId, Integer index);

    boolean existsById(String userId, String id);

    boolean existsByIndex(String userId, int index);

    Integer getSize(String userId);

}
