package ru.tsc.kitaev.tm.command.project;

import ru.tsc.kitaev.tm.command.AbstractProjectCommand;
import ru.tsc.kitaev.tm.enumerated.Sort;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListShowCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show project list...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        final List<Project> projects;
        System.out.println("[SHOW PROJECTS]");
        if (sort == null || sort.isEmpty()) projects = serviceLocator.getProjectService().findAll(userId);
        else {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = serviceLocator.getProjectService().findAll(userId, sortType.getComparator());
        }
        for (Project project: projects) {
            System.out.println(projects.indexOf(project) + 1 + ". " +project.toString() + ". " + project.getStatus());
        }
        System.out.println("[OK]");
    }

}
