package ru.tsc.kitaev.tm.command.project;

import ru.tsc.kitaev.tm.command.AbstractProjectCommand;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public final class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project by name...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter Name");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().removeByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectTaskService().removeById(userId, project.getId());
        System.out.println("[OK]");
    }

}
