package ru.tsc.kitaev.tm.command.system;

import ru.tsc.kitaev.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentsShowCommand extends AbstractCommand {

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String arg() {
        return "-arg";
    }

    @Override
    public String description() {
        return "Display list arguments...";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands) {
            String argument = command.arg();
            if (argument != null || !argument.isEmpty())
                System.out.println(argument + ": " + command.description());
        }
    }

}
