package ru.tsc.kitaev.tm.command.task;

import ru.tsc.kitaev.tm.command.AbstractTaskCommand;
import ru.tsc.kitaev.tm.enumerated.Sort;
import ru.tsc.kitaev.tm.model.Task;
import ru.tsc.kitaev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListShowCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task list...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        final List<Task> tasks;
        System.out.println("[SHOW PROJECTS]");
        if (sort == null || sort.isEmpty()) tasks = serviceLocator.getTaskService().findAll(userId);
        else {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = serviceLocator.getTaskService().findAll(userId, sortType.getComparator());
        }
        for (Task task: tasks) {
            System.out.println(tasks.indexOf(task) + 1 + ". " + task.toString() + ". " + task.getStatus());
        }
        System.out.println("[OK]");
    }

}
