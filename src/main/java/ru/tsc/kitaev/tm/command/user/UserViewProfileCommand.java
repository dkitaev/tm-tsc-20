package ru.tsc.kitaev.tm.command.user;

import ru.tsc.kitaev.tm.command.AbstractCommand;
import ru.tsc.kitaev.tm.model.User;

public class UserViewProfileCommand extends AbstractCommand {

    @Override
    public String name() {
        return "view-user";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "view user profile...";
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[VIEW PROFILE]");
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("E_MAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("[OK]");
    }

}
