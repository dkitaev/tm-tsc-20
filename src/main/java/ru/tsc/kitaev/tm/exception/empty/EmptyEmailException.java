package ru.tsc.kitaev.tm.exception.empty;

import ru.tsc.kitaev.tm.exception.AbstractException;

public final class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error. Email is empty.");
    }

    public EmptyEmailException(String value) {
        super("Error" + value + " Email is empty.");
    }

}
