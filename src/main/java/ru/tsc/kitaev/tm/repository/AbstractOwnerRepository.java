package ru.tsc.kitaev.tm.repository;

import ru.tsc.kitaev.tm.api.repository.IOwnerRepository;
import ru.tsc.kitaev.tm.model.AbstractOwnerEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    @Override
    public E add(final String userId, final E entity) {
        if (!userId.equals(entity.getUserId())) return null;
        list.add(entity);
        return entity;
    }

    @Override
    public void remove(final String userId, final E entity) {
        if (!userId.equals(entity.getUserId())) return;
            list.remove(entity);
    }

    @Override
    public void clear(final String userId) {
        final List<E> entity = findAll(userId);
        list.removeAll(entity);
    }

    @Override
    public List<E> findAll(final String userId) {
        final List<E> result = new ArrayList<>();
        for (final E entity : list) {
            if (userId.equals(entity.getUserId())) result.add(entity);
        }
        return result;
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        final List<E> entities = findAll(userId);
        entities.sort(comparator);
        return entities;
    }

    @Override
    public E findById(final String userId, final String id) {
        for (final E entity : list) {
            if (!userId.equals(entity.getUserId())) continue;
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        final List<E> entities = findAll(userId);
        return entities.get(index);
    }

    @Override
    public E removeById(final String userId, final String id) {
        final E entity = findById(userId, id);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        final E entity = findById(userId, id);
        return entity != null;
    }

    @Override
    public boolean existsByIndex(final String userId, final int index) {
        final E entity = findByIndex(userId, index);
        return entity != null;
    }

    @Override
    public Integer getSize(final String userId) {
        final List<E> entity = findAll(userId);
        return entity.size();
    }

}
