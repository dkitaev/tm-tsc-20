package ru.tsc.kitaev.tm.repository;

import ru.tsc.kitaev.tm.api.repository.IRepository;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class AbstractRepository <E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    @Override
    public E add(final E entity) {
        list.add(entity);
        return entity;
    }

    @Override
    public void remove(final E entity) {
        list.remove(entity);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public List<E> findAll() {
        return list;
    }

    @Override
    public List<E> findAll(Comparator<E> comparator) {
        final List<E> entities = new ArrayList<>(this.list);
        entities.sort(comparator);
        return entities;
    }

    @Override
    public E findById(final String id) {
        for (E entity: list) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E findByIndex(final Integer index) {
        return list.get(index);
    }

    @Override
    public E removeById(final String id) {
        final E entity = findById(id);
        if (entity == null) throw new ProjectNotFoundException();
        list.remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(final Integer index) {
        final E entity = findByIndex(index);
        if (entity == null) throw new ProjectNotFoundException();
        list.remove(entity);
        return entity;
    }

    @Override
    public boolean existsById(final String id) {
        final E entity = findById(id);
        return entity != null;
    }

    @Override
    public boolean existsByIndex(final int index) {
        if (index < 0) return false;
        return index < list.size();
    }

    @Override
    public Integer getSize() {
        return list.size();
    }

}
