package ru.tsc.kitaev.tm.repository;

import ru.tsc.kitaev.tm.api.repository.IUserRepository;
import ru.tsc.kitaev.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User removeUser(final User user) {
        list.remove(user);
        return user;
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user: list) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (final User user: list) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

}
