package ru.tsc.kitaev.tm.service;

import ru.tsc.kitaev.tm.api.repository.IOwnerRepository;
import ru.tsc.kitaev.tm.api.service.IOwnerService;
import ru.tsc.kitaev.tm.exception.empty.EmptyIdException;
import ru.tsc.kitaev.tm.exception.empty.EmptyIndexException;
import ru.tsc.kitaev.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kitaev.tm.model.AbstractOwnerEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E>
        implements IOwnerService<E> {

    protected final IOwnerRepository<E> ownerRepository;

    public AbstractOwnerService(final IOwnerRepository<E> ownerRepository) {
        super(ownerRepository);
        this.ownerRepository = ownerRepository;
    }

    @Override
    public E add(final String userId, final E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new RuntimeException();
        return ownerRepository.add(userId, entity);
    }

    @Override
    public void remove(final String userId, final E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new RuntimeException();
        ownerRepository.remove(userId, entity);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        ownerRepository.clear(userId);
    }

    @Override
    public List<E> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return ownerRepository.findAll(userId);
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return Collections.emptyList();
        return ownerRepository.findAll(userId, comparator);
    }

    @Override
    public E findById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return ownerRepository.findById(userId, id);
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return ownerRepository.findByIndex(userId, index);
    }

    @Override
    public E removeById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return ownerRepository.removeById(userId, id);
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return ownerRepository.removeByIndex(userId, index);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) return false;
        return ownerRepository.existsById(userId, id);
    }

    @Override
    public boolean existsByIndex(final String userId, final int index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return ownerRepository.existsByIndex(userId, index);
    }

    @Override
    public Integer getSize(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return ownerRepository.getSize(userId);
    }

}
