package ru.tsc.kitaev.tm.util;

import ru.tsc.kitaev.tm.exception.AbstractException;
import ru.tsc.kitaev.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() throws AbstractException {
        final String value = SCANNER.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (final RuntimeException e) {
            throw new IndexIncorrectException(value);
        }
    }

}
